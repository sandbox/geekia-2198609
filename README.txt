Tawea
---------------
This module installs in your site tawea-chat.

Install module:
---------------
1. Extract module archive in "sites/all/modules".
2. Enable module "tawea".
3. And chat ;).
